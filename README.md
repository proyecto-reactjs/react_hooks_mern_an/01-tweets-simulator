# Curso React JS Hooks: De Cero a Experto Creado Aplicaciones Reales

## Proyecto Simulador de Tweets

Instruido por:

Agustin Navarro Galdon

![Portada](https://i.imgur.com/Fm2jl2N.png)

## Nota:

creado con:

`npx create-react-app`

### Comandos:

- `npm install moment --save`
- `npm install --save-dev sass`
- `npm install @material-ui/core`
- `npm install @material-ui/icons`

Para el comando de sass debes instalar antes : `npm install -g sass` y luego `npm install --save-dev sass`
